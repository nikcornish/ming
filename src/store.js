import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    todos: [
      { id: 1, title: "First todo", description: "First todo description", completed: false, open: false },
      { id: 2, title: "Another todo", description: "Another todo description", completed: false, open: false },
      { id: 3, title: "Third todo", description: "Third todo description", completed: false, open: false }
    ]
  },
  getters: {
    todos: state => state.todos,
    open:  state => state.todos.filter(todo => todo.open == true),
    done:  state => state.todos.filter(todo => todo.completed == true),
    todocount: state => state.todos.length
  },
  mutations: {
    OPEN_TODO: (state, payload) => {
      state.todos.forEach(todo => todo.open = false)
      let target = state.todos.filter(todo => todo.id == payload.id)[0];
      target.open = true;
    },
    DELETE_TODO: (state, payload) => {
      let index = state.todos.findIndex(todo => todo.id == payload.id);
      state.todos.splice(index, 1);

    },
    DONE_TODO: (state, payload) => {
      let target = state.todos.filter(todo => todo.id == payload.id)[0];
      target.completed = true;
      target.open = false;
    },
    ADD_TODO: (state, payload) => {
      var newID = state.todos.length + 1,
          newTodo = {
            id: newID, 
            title: payload.title, 
            description: payload.description,
            completed: false,
            open: false
          };
      state.todos.push(newTodo);
    }
  },
  actions: {

  }
})
